FROM ubuntu:14.04
LABEL "Mongodb-version"="3.2.14 Communinity Edition"
LABEL "DevelopedBy"="Muhammad Afzal Qureshi"


RUN sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927 && \
    echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list && \
    sudo apt-get update && \
    sudo apt-get install -y mongodb-org=3.2.14 mongodb-org-server=3.2.14 mongodb-org-shell=3.2.14 mongodb-org-mongos=3.2.14 mongodb-org-tools=3.2.14 && \
    echo "mongodb-org hold" | dpkg --set-selections && echo "mongodb-org-server hold" | dpkg --set-selections && \
    echo "mongodb-org-shell hold" | dpkg --set-selections && \
    echo "mongodb-org-mongos hold" | dpkg --set-selections && \
    echo "mongodb-org-tools hold" | dpkg --set-selections

VOLUME /data/db

ENV AUTH yes
ENV STORAGE_ENGINE wiredTiger
ENV JOURNALING yes

ADD run.sh /run.sh
ADD set_mongodb_password.sh /set_mongodb_password.sh

EXPOSE 27017 28017

CMD ["./run.sh"]