## Mongo docker image with authentication

Follow the below commands to get started with it 

1. `docker pull registry.lendfoundry.com/lf-mongo:3.2`
1. `docker run -p 27017:27017 --restart always -e MONGODB_USER="admin" -e MONGODB_PASS="mypass" -d registry.lendfoundry.com/lf-mongo:3.2`